import numpy as np


class Experience:
    def __init__(self, params):
        self.width = params['in_width']
        self.height = params['in_height']
        self.max_size = params['hist_size']
        self.states = np.zeros([self.max_size, self.width, self.height, 4], dtype=np.uint8)
        self.actions = np.zeros([self.max_size])
        self.rewards = np.zeros([self.max_size])
        self.nstates = np.zeros([self.max_size, self.width, self.height, 4], dtype=np.uint8)
        self.terminals = np.zeros([self.max_size])

        self.count = 0

    def get_batch(self, size):
        # s = np.zeros([size, self.width, self.height, 4])
        # a = np.zeros([size])
        # r = np.zeros([size])
        # t = np.zeros([size])

        rands = np.random.randint(low=0, high=min(self.count, self.max_size), size=(size))
        s = self.states[rands, :, :, :]
        a = self.actions[rands]
        r = self.rewards[rands]
        ns = self.nstates[rands, :, :, :]
        t = self.terminals[rands]

        return s, a, r, ns, t

    def add(self, s, a, r, ns, t):
        index = self.count % self.max_size
        self.states[index, :, :, :] = s
        self.actions[index] = a
        self.rewards[index] = r
        self.nstates[index, :, :, :] = ns
        self.terminals[index] = t
        self.count += 1

