#! /usr/bin/env python2
from Agent import Agent
#import sacred

#ex = sacred.Experiment("atari-dqn")

net_params = {
    'conv_layers': 3,
    'conv_units': [32, 64, 64],
    'filter_size': [8, 4, 3],
    'strides': [4, 2, 1],
    'n_frames': 4,
    'fc_layers': 1,
    'fc_units': [512],
    'tensorboard': False,
    'log_freq': 10,
}

agent_params = {
    'vis': True,
    'eval': False,
    'random_seed': 42,
    'frame_skip': 4,
    'rom_path': '/home/rasmus/ale-git/roms/breakout.bin',
    'in_width': 84,
    'in_height': 84,
    'discount': 0.99,
    'eps': 1.0,
    'eps_min': 0.1,
    'eps_decay': 1e-6,
    'lr': 0.0001, # 0.00025,
    'ldecay': 0.0,
    'momentum': 0.95,
    'opt_eps': 1e-6,
    'num_episodes': 1000,
    'episode_len': 10000,
    'hist_size': 30000,
    'minibatch': 32,
    'save_interval': 5000,
    'sync_freq': 2500,
    'learn_start': 5000,
    #'test_freq': 5000,
    #'num_test_ep': 5,
}


#@ex.automain
def main():
    params = net_params.copy()
    params.update(agent_params)
    agent = Agent(params)

    if params['eval']:
        agent.dqn.restore('net/model_45000')
        for i in range(10):
            agent.new_game()
            agent.run_episode(params)
    else:
        #agent.dqn.restore('net/model_30000')  # train from previous model
        agent.train(params)

main()
