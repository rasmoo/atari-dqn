import cv2
import numpy as np
from ale_python_interface import ALEInterface
from DQN import DQN
from Experience import Experience
import time


class Agent:
    """ Agent
    """
    def __init__(self, params):
        self.ale = ALEInterface()
        self.ale.setInt('random_seed', params['random_seed'])
        self.ale.setInt('frame_skip', params['frame_skip'])
        self.ale.setBool('color_averaging', True)
        self.ale.loadROM(params['rom_path'])
        self.legal_actions = self.ale.getMinimalActionSet()
        params['num_actions'] = len(self.legal_actions)
        self.action_dict = dict()
        for i in range(len(self.legal_actions)):
            self.action_dict[self.legal_actions[i]] = i
        print self.action_dict

        (self.screen_width, self.screen_height) = self.ale.getScreenDims()
        (self.display_width, self.display_height) = (2*160, 2*210)

        self.image_buffer = np.zeros((1, self.screen_height, self.screen_width, 4))

        self.dqn = DQN(params)
        self.experience = Experience(params)

        self.frame_count = 0

        if params['vis']:
            cv2.startWindowThread()
            cv2.namedWindow('atari')
            cv2.namedWindow('viz')

    def onehot_actions(self, actions):
        batchsize = len(actions)
        num_act = len(self.action_dict)
        onehot = np.zeros((batchsize, num_act))
        for i in range(batchsize):
            onehot[i][self.action_dict[int(actions[i])]] = 1
        return onehot

    def get_screen(self):
        numpy_surface = np.zeros(self.screen_height * self.screen_width * 3, dtype=np.uint8)
        self.ale.getScreenRGB(numpy_surface)
        image = np.reshape(numpy_surface, (self.screen_height, self.screen_width, 3))
        return image

    def next(self, action):
        reward = self.ale.act(action)
        next_screen = self.get_screen()
        next_screen = cv2.cvtColor(next_screen, cv2.COLOR_BGR2GRAY)
        return reward, next_screen

    def new_game(self):
        self.ale.reset_game()

    def select_action(self, eps, state): # eps-greedy action selection
        if np.random.rand() > eps:
            Q = self.dqn.run(state)
            print Q
            a = np.argmax(Q)
            act = self.legal_actions[a]
            """
            if act == 3:
                print '->'
            elif act == 4:
                print '<-'
            elif act == 0:
                print 'v'
            elif act == 1:
                print '^'
            """

            return act
        else:
            return self.legal_actions[np.random.randint(0, len(self.legal_actions))]

    @staticmethod
    def preprocess(frames, w, h):
        s = np.zeros([1, w, h, 4])
        for i in range(4):
            s[0, :, :, i] = cv2.resize(frames[0, :, :, i], (w, h))

        return s

    def train(self, params):
        for num_ep in range(params['num_episodes']):
            self.new_game()
            self.train_episode(params)

    def update_buffer(self, new_frame):
        self.image_buffer = np.roll(self.image_buffer, 1, axis=3)
        self.image_buffer[0, :, :, 0] = new_frame

    def act_perceive(self, params, phi_t):
        a = self.select_action(params['eps'], phi_t)
        r, new_frame = self.next(a)
        self.update_buffer(new_frame)
        phi_t1 = self.preprocess(self.image_buffer, params['in_width'], params['in_height'])
        t = self.ale.game_over()
        self.experience.add(phi_t, a, r, phi_t1, t)

        return phi_t1

    def train_episode(self, params):
        """ This is an implementation of Algorithm 1 from Mnih et al.
        """

        first_frame = self.get_screen()
        self.image_buffer[0, :, :, 0] = cv2.cvtColor(first_frame, cv2.COLOR_BGR2GRAY)
        phi_t1 = self.preprocess(self.image_buffer, params['in_width'], params['in_height'])

        for num_frame in range(params['episode_len']):
            phi_t = phi_t1.copy()

            # TODO: why is action selection behaving differently during training and evaluation?

            phi_t1 = self.act_perceive(params, phi_t)

            # TODO: this might be implemented better
            # run test episodes
            #if self.frame_count % params['test_freq']:
            #    for i in range(params['num_test_ep']):
            #        self.new_game()
            #        self.run_episode()
            #    self.frame_count += 1 # just add one to not repeat testing
            #    return

            if self.frame_count >= params['learn_start']:
                # sample minibatch
                bs, ba, br, bns, bt = self.experience.get_batch(params['minibatch'])

                # optimize
                cost = self.dqn.train(bs, self.onehot_actions(ba), br, bns, bt, self.frame_count % params['log_freq'] == 0)
                #print cost

                # save
                if self.frame_count % params['save_interval'] == 0:
                    self.dqn.save('net/model_' + str(self.frame_count))
                    print 'Saved @ frame ' + str(self.frame_count)

                # sync Q_hat = Q
                if self.frame_count % params['sync_freq'] == 0:
                    self.dqn.sync_weights()
                    print 'Synced weights'

                # update eps
                params['eps'] = max(params['eps_min'], params['eps'] - params['eps_decay'])

            self.frame_count += 1

            # visualize
            if params['vis']:
                # vis_image = cv2.resize(x_t1, (self.display_width, self.display_height))
                vis_image = cv2.resize(self.image_buffer[0, :, :, :], (self.display_width, self.display_height))
                cv2.imshow('atari', vis_image)

            # new state is terminal?
            if self.ale.game_over():
                return

    def run_episode(self, params):
        first_frame = self.get_screen()
        self.image_buffer[0, :, :, 0] = cv2.cvtColor(first_frame, cv2.COLOR_BGR2GRAY)
        phi_t1 = self.preprocess(self.image_buffer, params['in_width'], params['in_height'])

        for num_frame in range(params['episode_len']):
            phi_t = phi_t1.copy()

            phi_t1 = self.act_perceive(params, phi_t)

            # visualize
            if params['vis']:
                #vis_image = cv2.resize(x_t1, (self.display_width, self.display_height))
                vis_image = cv2.resize(self.image_buffer[0, :, :, :], (self.display_width, self.display_height))
                cv2.imshow('atari', vis_image)

                #conv = self.dqn.get_activation(phi_t1)
                #print conv.shape
                #conv_image = conv[0, :, :, 5]
                #conv_image = cv2.resize(conv_image, (128, 128))
                #cv2.imshow('viz', conv_image)
                time.sleep(0.01)

            self.frame_count += 1
            # new state is terminal?
            if self.ale.game_over():
                break

