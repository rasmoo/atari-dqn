import tensorflow as tf
import cv2
import numpy as np


class DQN:
    """
    Documentation
    """

    @staticmethod
    def make_weight(shape):
        return tf.get_variable('weight', shape,
                               initializer=tf.truncated_normal_initializer(stddev=0.001))

    @staticmethod
    def make_bias(shape):
        return tf.get_variable('bias', shape,
                               initializer=tf.constant_initializer(0.01))

    @staticmethod
    def conv2d(x, W, stride):
        return tf.nn.conv2d(x, W, strides=[1, stride, stride, 1], padding="SAME")

    @staticmethod
    def max_pool_2x2(x):
        return tf.nn.max_pool(x, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding="SAME")

    def run(self, state):
        return self.sess.run(self.Q, feed_dict={self.state: state,
                                                self.nstate: np.zeros([1, 84, 84, 4]),
                                                self.rewards: [0],
                                                self.actions: np.zeros([1, 4]),
                                                self.terminals: [0]})

    def sync_weights(self):
        return self.sess.run(self.assign_ops)


    def __init__(self, params):
        # Tensorflow init
        tf.set_random_seed(params['random_seed'])
        self.sess = tf.Session()

        # placeholders
        self.state = tf.placeholder("float", [None, params['in_width'], params['in_height'], 4], name='state')
        self.nstate = tf.placeholder("float", [None, params['in_width'], params['in_height'], 4], name='nstate')
        self.rewards = tf.placeholder("float", [None], name='rewards')
        self.actions = tf.placeholder("float", [None, params['num_actions']], name='actions')  # one-hot because Tensorflow...
        self.terminals = tf.placeholder("float", [None], name='terminals')

        outputs = [self.state]
        outputs_hat = [self.nstate] # _hat =target network
        self.assign_ops = []

        # make conv layers
        for n in range(params['conv_layers']):
            with tf.variable_scope('conv' + str(n)) as scope:
                shape = [params['filter_size'][n],
                         params['filter_size'][n],
                         params['n_frames'] if n == 0 else params['conv_units'][n-1],
                         params['conv_units'][n]]
                W = self.make_weight(shape)
                b = self.make_bias(params['conv_units'][n])
                conv = self.conv2d(outputs[-1], W, params['strides'][n])
                conv = tf.nn.bias_add(conv, b)
                conv = tf.nn.relu(conv, name=scope.name)
                outputs.append(conv)

                # target network and assign ops
                W_hat = tf.Variable(W.initialized_value(), trainable=False)
                b_hat = tf.Variable(b.initialized_value(), trainable=False)
                conv_hat = self.conv2d(outputs_hat[-1], W_hat, params['strides'][n]) + b_hat
                conv_hat = tf.nn.bias_add(conv_hat, b_hat)
                conv_hat = tf.nn.relu(conv_hat, name=scope.name)
                outputs_hat.append(conv_hat)
                W_op = W_hat.assign(W)
                b_op = b_hat.assign(b)
                self.assign_ops.append(W_op)
                self.assign_ops.append(b_op)

                # tensorboard logging ops
                #W_hist = tf.histogram_summary(scope.name + "_W", W)
                #b_hist = tf.histogram_summary(scope.name + "_b", b)
                #W_hat_hist = tf.histogram_summary(scope.name + "_W_hat", W_hat)
                #b_hat_hist = tf.histogram_summary(scope.name + "_b_hat", b_hat)

                sizes = [21, 11, 11] # hard coded :))
                cy = [4, 8, 8]
                cx = [8, 8, 8]
                I1=tf.slice(conv, (0,0,0,0), (1,-1,-1,-1))
                I1=tf.reshape(I1, (sizes[n], sizes[n], params['conv_units'][n]))
                I1 = tf.image.resize_image_with_crop_or_pad(I1, sizes[n]+4, sizes[n]+4)
                I1 = tf.reshape(I1, (sizes[n]+4, sizes[n]+4, cy[n], cx[n]))
                I1 = tf.transpose(I1, (2,0,3,1))
                I1 = tf.reshape(I1,(1, cy[n]*(sizes[n]+4), cx[n]*(sizes[n]+4), 1))
                images_after_conv_i=tf.image_summary(scope.name + "_activation", I1)

        # make fc layers
        dim = 1
        for d in outputs[-1].get_shape()[1:].as_list():
            dim *= d
        reshape = tf.reshape(outputs[-1], [-1, dim], name='reshape')
        outputs.append(reshape)

        reshape_hat = tf.reshape(outputs_hat[-1], [-1, dim], name='reshape_hat')
        outputs_hat.append(reshape_hat)

        for n in range(params['fc_layers']):
            with tf.variable_scope('fc' + str(n)) as scope:
                shape = [dim if n == 0 else params['fc_units'][n-1],
                         params['fc_units'][n]]
                W = self.make_weight(shape)
                b = self.make_bias(params['fc_units'][n])
                fc = tf.nn.relu_layer(outputs[-1], W, b, name=scope.name)
                outputs.append(fc)

                # target network and assign ops
                W_hat = tf.Variable(W.initialized_value(), trainable=False)
                b_hat = tf.Variable(b.initialized_value(), trainable=False)
                fc_hat = tf.nn.relu_layer(outputs_hat[-1], W_hat, b_hat, name=scope.name)
                outputs_hat.append(fc_hat)
                W_op = W_hat.assign(W)
                b_op = b_hat.assign(b)
                self.assign_ops.append(W_op)
                self.assign_ops.append(b_op)

                #fc_hist = tf.histogram_summary(scope.name + "_out", fc)
                #W_hist = tf.histogram_summary(scope.name + "_W", W)
                #b_hist = tf.histogram_summary(scope.name + "_b", b)
                #W_hat_hist = tf.histogram_summary(scope.name + "_W_hat", W_hat)
                #b_hat_hist = tf.histogram_summary(scope.name + "_b_hat", b_hat)

        # output layer
        with tf.variable_scope('output') as scope:
            shape = [params['fc_units'][-1],
                     params['num_actions']]
            W = self.make_weight(shape)
            b = self.make_bias(params['num_actions'])
            self.Q = tf.nn.bias_add(tf.matmul(outputs[-1], W), b, name=scope.name+'_Q')
            self.max_Q = tf.reduce_max(self.Q, 1, name=scope.name+'_max_Q')
            outputs.append(self.Q)
            outputs.append(self.max_Q)

            # target network and assign ops
            W_hat = tf.Variable(W.initialized_value(), trainable=False)
            b_hat = tf.Variable(b.initialized_value(), trainable=False)
            self.Q_hat = tf.nn.bias_add(tf.matmul(outputs_hat[-1], W_hat), b_hat, name=scope.name+'_Q_hat')
            self.max_Q_hat = tf.reduce_max(self.Q_hat, 1, name=scope.name+'_max_Q_hat')
            outputs_hat.append(self.Q_hat)
            outputs_hat.append(self.max_Q_hat)
            W_op = W_hat.assign(W)
            b_op = b_hat.assign(b)
            self.assign_ops.append(W_op)
            self.assign_ops.append(b_op)

            # tensorboard logging
            #W_hist = tf.histogram_summary(scope.name + "_W", W)
            #b_hist = tf.histogram_summary(scope.name + "_b", b)
            max_Q_hist = tf.histogram_summary(scope.name + "_max_Q", self.max_Q)
            max_Q_hat_hist = tf.histogram_summary(scope.name + "_max_Q_hat", self.max_Q_hat)

        # cost
        self.discount = tf.constant(params['discount'])
        self.y_j = tf.add(self.rewards, tf.mul(self.discount, tf.mul(tf.sub(1.0, self.terminals), self.max_Q_hat)))
        # self.Q = self.y[:, actions] <-- not possible, see https://github.com/tensorflow/tensorflow/issues/206
        self.Q_pred = tf.reduce_sum(tf.mul(self.Q, self.actions), 1)  # actions must be one-hot encoded
        self.num_act = params['num_actions']
        self.cost = tf.reduce_sum(tf.pow(tf.sub(self.y_j, self.Q_pred), 2))

        # optimizer
        self.optimize_op = tf.train.RMSPropOptimizer(params['lr'], params['ldecay'], params['momentum'],
                                                     params['opt_eps']).minimize(self.cost)
        #self.optimize_op = tf.train.AdagradOptimizer(params['lr']*20).minimize(self.cost)

        self.saver = tf.train.Saver()

        self.sess.run(tf.initialize_all_variables())

        self.logging = False
        if params['tensorboard']:
            self.logging = True
            self.merged = tf.merge_all_summaries()
            self.writer = tf.train.SummaryWriter("logs/", self.sess.graph_def)

    def train(self, s, a, r, ns, t, log):
        feed_dict = {self.state: s, self.actions: a, self.rewards: r, self.nstate: ns, self.terminals: t}

        if self.logging and log:
            cost, summary, _ = self.sess.run([self.cost, self.merged, self.optimize_op], feed_dict)
            self.writer.add_summary(summary)
        else:
            cost, _ = self.sess.run([self.cost, self.optimize_op], feed_dict)
        return cost

    def save(self, path):
        self.saver.save(self.sess, path)

    def restore(self, path):
        self.saver.restore(self.sess, path)
